
CREATE TABLE srcaltcatalogo (
                codigo INTEGER NOT NULL,
                valor CHAR(3) NOT NULL,
                descripcion VARCHAR(100) NOT NULL,
                estado bool NOT NULL,
                orden INTEGER,
                CONSTRAINT srcalrcatalogopk PRIMARY KEY (codigo, valor)
);


CREATE TABLE srauttroles (
                nombrerol VARCHAR(50) NOT NULL,
                estado bool NOT NULL,
                fechacreacion TIMESTAMP NOT NULL,
                fechamodificacion TIMESTAMP,
                CONSTRAINT srautrrolespk PRIMARY KEY (nombrerol)
);


CREATE TABLE srauttusuario (
                idusuario INTEGER NOT NULL,
                nombreusuario VARCHAR(50) NOT NULL UNIQUE,
                password VARCHAR(500) NOT NULL,
                nombres VARCHAR(50),
                apellidos VARCHAR(50),
                email VARCHAR(100),
                estado bool NOT NULL,
                fechacreacion TIMESTAMP NOT NULL,
                fechamodificacion TIMESTAMP,
                CONSTRAINT srautrusuariopk PRIMARY KEY (idusuario)
);


CREATE TABLE srcaltpersona (
                idpersona INTEGER NOT NULL,
                nombres VARCHAR(50) NOT NULL,
                apellidos VARCHAR(50) NOT NULL,
                email VARCHAR(100),
                cedula VARCHAR(10),
                estado bool NOT NULL,
                fecharegistro TIMESTAMP NOT NULL,
                idusuarioregistro INTEGER NOT NULL,
                fechamodificacion TIMESTAMP,
                idusuariomodificacion INTEGER,
                CONSTRAINT srcalrpersonapk PRIMARY KEY (idpersona)
);

CREATE TABLE srauttrolesusuario (
                nombrerol VARCHAR(50) NOT NULL,
                idusuario INTEGER NOT NULL,
                estado bool NOT NULL,
                fechacreacion TIMESTAMP NOT NULL,
                fechamodificacion TIMESTAMP,
                CONSTRAINT srautrrolesusuariopk PRIMARY KEY (nombrerol, idusuario)
);

ALTER TABLE srauttrolesusuario ADD CONSTRAINT srautrusuariorolespfk
FOREIGN KEY (nombrerol)
REFERENCES srauttroles (nombrerol);

ALTER TABLE srauttrolesusuario ADD CONSTRAINT srautrrolesusuariopfk
FOREIGN KEY (idusuario)
REFERENCES srauttusuario (idusuario);

ALTER TABLE srcaltpersona ADD CONSTRAINT srcalrusuariopkpersonafk1
FOREIGN KEY (idusuarioregistro)
REFERENCES srauttusuario (idusuario);

ALTER TABLE srcaltpersona ADD CONSTRAINT srcalrusuariopkpersonafk
FOREIGN KEY (idusuariomodificacion)
REFERENCES srauttusuario (idusuario);

-----data-----
INSERT INTO srauttroles(nombrerol, estado, fechacreacion, fechamodificacion)
VALUES('ROLE_USER', '1', current_timestamp, NULL);

INSERT INTO srauttroles(nombrerol, estado, fechacreacion, fechamodificacion)
VALUES('ROLE_ADMIN', '1', current_timestamp, NULL);

INSERT INTO srauttusuario
(idusuario, nombreusuario, password, nombres, apellidos, email, estado, fechacreacion, fechamodificacion)
VALUES(1, 'admin', '$2a$10$XURPShQNCsLjp1ESc2laoObo9QZDhxz73hJPaEv7/cBha4pk0AgP.', 'KILLARY VALENTINA', 'IZA CHILIQUINGA', 'inkarri.programmer@gmail.com', '1', current_timestamp, NULL);

INSERT INTO srauttrolesusuario
(nombrerol, idusuario, estado, fechacreacion, fechamodificacion)
VALUES('ROLE_ADMIN', 1, '1', current_timestamp, NULL);

INSERT INTO public.srcaltpersona
(idpersona, nombres, apellidos, email, cedula, estado, fecharegistro, idusuarioregistro, fechamodificacion, idusuariomodificacion)
VALUES(1, 'Hannah', 'Arendt', 'inkarri666@gmail.com', '0508976528', 'true', current_timestamp, 1, NULL, NULL);


